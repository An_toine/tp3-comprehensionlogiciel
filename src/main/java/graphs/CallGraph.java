package graphs;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.LongStream;

import loggers.ConsoleLogger;
import loggers.FileLogger;
import loggers.LogRequest;
import loggers.StandardLogRequestLevel;
import processors.ASTProcessor;

public abstract class CallGraph extends ASTProcessor {
	/* ATTRIBUTES */
	private Set<String> methods = new HashSet<>();
	private Map<String, Map<String, Integer>> invocations = new HashMap<>();
	//
	private Map<String, Map<String, Integer>> invocationsClass = new HashMap<>();
	//
	private FileLogger loggerChain;
	
	/* CONSTRUCTOR */
	public CallGraph(String projectPath) {
		super(projectPath);
		setLoggerChain();
	}
	
	/* METHODS */
	protected void setLoggerChain() {
		loggerChain = new FileLogger(StandardLogRequestLevel.DEBUG);
		loggerChain.setNextLogger(new ConsoleLogger(StandardLogRequestLevel.INFO));
	}
	
	public Set<String> getMethods() {
		return methods;
	}
	
	public long getNbMethods() {
		return methods.size();
	}
	
	public long getNbInvocations() {
		return invocations.keySet()
		.stream()
		.map(source -> invocations.get(source))
		.map(destination -> destination.values())
		.flatMap(Collection::stream)
		.flatMapToLong(value -> LongStream.of((long) value))
		.sum();
	}
	
	public Map<String, Map<String, Integer>> getInvocations() {
		return invocations;
	}
	
	//
	public Map<String, Map<String, Integer>> getInvocationsClass() {
		return invocationsClass;
	}
	//
	
	public void fillTest() {
		invocationsClass = new HashMap<>();
		invocationsClass.put("A", new HashMap<>());
		invocationsClass.put("B", new HashMap<>());
		invocationsClass.put("C", new HashMap<>());
		invocationsClass.put("D", new HashMap<>());
		invocationsClass.put("E", new HashMap<>());
		
		invocationsClass.get("A").put("B", 5);
		invocationsClass.get("A").put("D", 4);
		invocationsClass.get("A").put("E", 8);
		
		invocationsClass.get("B").put("A", 10);
		invocationsClass.get("B").put("D", 4);
		invocationsClass.get("B").put("C", 2);
		
		invocationsClass.get("C").put("E", 6);
		
		invocationsClass.get("D").put("A", 5);
		invocationsClass.get("D").put("B", 5);
		
		invocationsClass.get("E").put("B", 8);	
	}
	
	public boolean addMethod(String method) {
		return methods.add(method);
	}
	
	public boolean addMethods(Set<String> methods) {
		return methods.addAll(methods);
	}
	
	public void addInvocation(String source, String destination, String classDestination) {
		
		if (invocations.containsKey(source)) {
			
			if (invocations.get(source).containsKey(destination)) {
				int numberOfArrows = invocations.get(source).get(destination);
				invocations.get(source).put(destination, numberOfArrows + 1);
				//
				invocationsClass.get(source).put(classDestination, numberOfArrows + 1);
				//
			}
			
			else {
				methods.add(destination);
				invocations.get(source).put(destination, 1);
				//
				invocationsClass.get(source).put(classDestination, 1);
				//
			}
		}
		
		else {
			methods.add(source);
			methods.add(destination);
			invocations.put(source, new HashMap<String, Integer>());
			invocations.get(source).put(destination, 1);
			//
			invocationsClass.put(source, new HashMap<String, Integer>());
			invocationsClass.get(source).put(classDestination, 1);
			//
		}
	}
	 
	public void addInvocation(String source, String destination, int occurrences) {
		methods.add(source);
		methods.add(destination);
		
		if (!invocations.containsKey(source))
			invocations.put(source, new HashMap<String, Integer>());
		
		invocations.get(source).put(destination, occurrences);
	}
	
	public void addInvocationClass(String sourceClass, String destinationClass, int occurrences) {
		if (!invocationsClass.containsKey(sourceClass))
			invocationsClass.put(sourceClass, new HashMap<String, Integer>());
		
		invocationsClass.get(sourceClass).put(destinationClass, occurrences);
	}
	
	public void addInvocationsClass(Map<String, Map<String, Integer>> mapClass) {
		for (String source: mapClass.keySet())
			for (String destination: mapClass.get(source).keySet())
				this.addInvocationClass(source, destination, mapClass.get(source).get(destination));
	}
	
	public void addInvocations(Map<String, Map<String, Integer>> map) {
		for (String source: map.keySet())
			for (String destination: map.get(source).keySet())
				this.addInvocation(source, destination, map.get(source).get(destination));
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Static Call Graph");
		builder.append("\nMethods: "+methods.size()+".");
		builder.append("\nInvocations: "+getNbInvocations()+".");
		builder.append("\n");
		
		for (String source: invocations.keySet()) {
			builder.append(source + ":\n");
			
			for (String destination: invocations.get(source).keySet())
				builder.append("\t---> " + destination + 
						" (" + invocations.get(source).get(destination) + " time(s))\n");
			builder.append("\n");
		}
		
		return builder.toString();
	}
	
	public void print() {
		loggerChain.log(new LogRequest(this.toString(), 
				StandardLogRequestLevel.INFO));
	}
	
	public void log() {
		loggerChain.setFilePath(parser.getProjectPath()+"static-callgraph.info");
		loggerChain.log(new LogRequest(this.toString(), 
				StandardLogRequestLevel.DEBUG));
	}
}
