package graphs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Couplage {

	//Chaque Classe a une list de classe avec le nombre d'appel vers telle classe
	private int nbTotalAppel = 0;
	private Map<String,Map<String,Integer>> mapAppels;
	private ArrayList<Couple> listCouplesPossiblesPonderationInit;
	private ArrayList<Couple> listCouplesPossiblesPonderationTmp;
	private Map<String, Boolean> classCoupled;
	private ArrayList<Cluster> listCluster;
	//D:\JavaWork\HMIN233A_part2\src\
	
	public Couplage(Map<String,Map<String,Integer>> mapApp) {
		this.mapAppels = mapApp;
		this.listCouplesPossiblesPonderationInit = new ArrayList<Couple>();
		this.listCouplesPossiblesPonderationTmp = new ArrayList<Couple>();
		this.classCoupled = new HashMap<String,Boolean>();
		this.listCluster = new ArrayList<Cluster>();
		this.calculNbAppelsTotalInit();
	}
	
	public void clusteringInit() {
		
	}
	/////////////////////////////////////////////////////////
	public ArrayList<Cluster> clustering() {
		//on r�cup�res les clusters d�j� existants
		ArrayList<Cluster> arbre  = this.listCluster;
		Map<KeyCouple,Integer> listClustAppel;
		KeyCouple bestCouple;
		int bestPoids = 0;
		while(listCluster.size()>1) {
			listClustAppel = clusterProche();
			for(Map.Entry<KeyCouple,Integer> entry : listClustAppel.entrySet()) {
				if(entry.getValue()>bestPoids) {
					bestCouple = entry.getKey();
					bestPoids = entry.getValue();
				}
			}
			arbre.add(new Cluster())
			this.calculNbAppelsTotalExterneClustering();
		}
		return arbre;
	}
	
	//obtenir le nombre d'appels avec deux classes
	public int getNbAppel(String class1, String class2) {
		int nbCall = 0;
		if(mapAppels.containsKey(class1) && mapAppels.get(class1).containsKey(class2)) {
			nbCall += mapAppels.get(class1).get(class2);
		}
		if(mapAppels.containsKey(class2) && mapAppels.get(class2).containsKey(class1)) {
			nbCall += mapAppels.get(class2).get(class1);
		}
		return nbCall;
	}
	
	//Renvoie une map de combinaisons de cluster ponderes
	public Map<KeyCouple,Integer> clusterProche() {
		Map<KeyCouple,Integer> couplesCluster = new HashMap<>();
		int c1, c2, i, nbCall;
		Cluster clust;
		//parcours de la liste de clusters pour chaque couples possibles
		for(Couple couple : listCouplesPossiblesPonderationTmp) {
			c1 = -1;
			c2 = -1;
			i = 0;
			//calcul nombre d'appels pour couple
			nbCall = this.getNbAppel(couple.getCouple().getSource(),couple.getCouple().getDestination());
			while(i < listCluster.size() && c1 == -1 && c2 == -1) {
				clust = listCluster.get(i);
				if(clust.containsClass(couple.getCouple().getSource())) c1 = clust.getNumeroCluster();
				else if(clust.containsClass(couple.getCouple().getDestination())) c2 = clust.getNumeroCluster();
			}
			
			//Si classes coutenues dans MapAppels
			if(c1 > -1 && c2 > -1) {
				KeyCouple kc = new KeyCouple(Integer.toString(c1), Integer.toString(c2));
				//S'il y a deja le couple ou son inverse dans map de combinaisons de cluster ponderes
				if(couplesCluster.containsKey(kc)) {
					nbCall += couplesCluster.get(kc);
					couplesCluster.put(kc, nbCall);
				}
				else if(couplesCluster.containsKey(this.getReverseKeyCouple(kc))) {
					nbCall += couplesCluster.get(this.getReverseKeyCouple(kc));
					couplesCluster.put(this.getReverseKeyCouple(kc), nbCall);
				}
				//S'il n'y a pas deja le couple dans map de combinaisons de cluster ponderes
				else {
					couplesCluster.put(kc, nbCall);
				}
			}
		}
		return couplesCluster;
	}
	
	public void supprCoupleUtilise(Couple couple) {
		listCouplesPossiblesPonderationTmp.remove(couple);
	}
	
	public void calculNbAppelsTotalExterneClustering() {
		int nbCall = 0;
		for(Couple c : listCouplesPossiblesPonderationTmp) {
			nbCall += mapAppels.get(c.getCouple().getSource()).get(c.getCouple().getDestination());
			// si l'inverse existe aussi
			if(mapAppels.containsKey(c.getCouple().getDestination()) && mapAppels.get(c.getCouple().getDestination()).containsKey(c.getCouple().getSource())) {
				nbCall += mapAppels.get(c.getCouple().getDestination()).get(c.getCouple().getSource());
			}
		}
		nbTotalAppel = nbCall;
	}
	
	public void calculNbAppelsTotalInit() {
		for(Map.Entry<String,Map<String,Integer>> entry : mapAppels.entrySet()) {
		    HashMap<String, Integer> listAppel = (HashMap<String, Integer>) entry.getValue();
		    for(Map.Entry<String,Integer> call : listAppel.entrySet()) {
		    	nbTotalAppel += call.getValue();
		    }
		}
		System.out.println("nb appels total: " + nbTotalAppel);
	}

	//renvoi une liste de couplage, il peut y avoir des classes seules, ceci se r�gle au niveau des clusters
	public ArrayList<Couple> couplage() {
		ArrayList<Couple> listCouple = new ArrayList<Couple>();
		String source, destination;
		boolean cSource, cDestination;
		//creation map couples possibles ponderes
		this.createPonderationCouple();
		//parcours pour creation couples
		for(Couple c : listCouplesPossiblesPonderationInit) {
			source = c.getCouple().getSource();
			destination = c.getCouple().getDestination();
			//
			cSource = classCoupled.containsKey(source);
			cDestination = classCoupled.containsKey(destination);
			//
			if(!((cSource && classCoupled.get(source)) || (cDestination && classCoupled.get(destination)))){
				listCouple.add(c);
				classCoupled.put(source,true);
				classCoupled.put(destination,true);
			}
			//pour garder en m�moire les classes potentiellement non couplees
			else {
				if(!cSource || cSource && !classCoupled.get(source)) {
					classCoupled.put(source,false);
				}
				if(!cDestination || cDestination && !classCoupled.get(destination)) {
					classCoupled.put(destination,false);
				}
			}
		}
		this.printCouplesPossiblesPondere();
		this.printClassCoupled(listCouple);
		return listCouple;
	}
	
	//on fait une ponderation de tous les couples possibles
	public void createPonderationCouple() {
		KeyCouple keyCouple, reverseKey;
		Map<KeyCouple,Integer> map = new HashMap<KeyCouple,Integer>();
		
		//remplissage d'une map temporaire qui contient nbAppel entre couples de classe
		for(Map.Entry<String,Map<String,Integer>> entry : mapAppels.entrySet()) {
		    String classe = entry.getKey();
		    HashMap<String, Integer> listAppel = (HashMap<String, Integer>) entry.getValue();
		    for(Map.Entry<String,Integer> call : listAppel.entrySet()) {
			    String classCall = call.getKey();
			    int nbCall = call.getValue();
			    keyCouple = new KeyCouple(classe,classCall);
			    reverseKey = getReverseKeyCouple(keyCouple);
			    //On check par exemple si B-A existe d�j� avant d'ajouter A-B sinon la map est fausse
			    if(map.containsKey(reverseKey)){
			    	int val = map.get(reverseKey);
			    	map.put(reverseKey, val + nbCall);
			    }
			    else {
			    	map.put(keyCouple, nbCall);
			    }
		    }
		}
		//On rempli cette list avec toutes les pond�rations de tous les couples possibles, trie du plus grand au plus petit quotient
		for(Map.Entry<KeyCouple,Integer> entry : map.entrySet()) {
			KeyCouple couple = entry.getKey();
			int nbAppelCouple = entry.getValue();
			addCouple(listCouplesPossiblesPonderationInit, new Couple(couple,calcQuotient(nbAppelCouple)));
		}
		listCouplesPossiblesPonderationTmp =  (ArrayList<Couple>) listCouplesPossiblesPonderationInit.clone();
		
	}
	
	public void addCouple(ArrayList<Couple> list, Couple couple) {
		int i = 0; 
		while(i<list.size() && couple.getPoids() < list.get(i).getPoids()) {
			++i;
		}
		list.add(i, couple);
	}
	
	//renvoie le quotient de couplage � partir d'un entier
	public float calcQuotient(float nbCall) {
		
		System.out.println(nbCall);
		System.out.println(nbTotalAppel);
		System.out.println("res: " + nbCall/nbTotalAppel);
		return nbCall/nbTotalAppel;
	}
	
	//donne l'inverse d'un couple, par exemple si A-B alors renvoi B-A
	public KeyCouple getReverseKeyCouple(KeyCouple kc) {
		return new KeyCouple(kc.getDestination(),kc.getSource());
	}
		
	/*
	//Incremente le nombre d'appels ou ajoute une classe cible pour une clase donne
	public void addCouple(String source, String destination, int nbAppel) {
		//Si class source deja dans map
		if(this.mapAppels.containsKey(source)) {
			//S'il y a deja un/des appel(s) a classe destination
			if(this.mapAppels.get(source).containsKey(destination)) {
				int val = this.mapAppels.get(source).get(destination);
				this.mapAppels.get(source).put(destination, val+nbAppel);
			}
			else {
				this.mapAppels.get(source).put(destination, nbAppel);
			}
		}
		else {
			Map<String,Integer> map = new HashMap<String,Integer>();
			map.put(destination, nbAppel);
			this.mapAppels.put(source, map);
		}
		nbTotalAppel+=nbAppel;
	}
	*/
	
	//print couplage possibles pond�r�s
		public void printCouplesPossiblesPondere() {
			System.out.println("------------- LISTE COUPLES POSSIBLES PONDERES -------------");
			for(Couple c : listCouplesPossiblesPonderationInit) {
				System.out.println("Classe 1: " + c.getCouple().getSource() + "; Classe 2: " + c.getCouple().getDestination() + "; poids: " + c.getPoids() + ".");
			}
			
		}
	
	//print couplage
	public void printClassCoupled(ArrayList<Couple> listCouple) {
		System.out.println("------------- LISTE COUPLES PONDERES POST-COUPLAGE -------------");
		for(Couple c : listCouple) {
			System.out.println("Classe 1: " + c.getCouple().getSource() + "; Classe 2: " + c.getCouple().getDestination() + "; poids: " + c.getPoids() + ".");
		}
		
	}
	
	//Getter et Setter
	
	public Map<String, Map<String, Integer>> getMapAppels() {
		return mapAppels;
	}

	public void setMapAppels(Map<String, Map<String, Integer>> mapCouples) {
		this.mapAppels = mapCouples;
	}
	
}
