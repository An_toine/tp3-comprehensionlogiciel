package graphs;

import java.util.ArrayList;

public class Cluster {
	private static int nbTotalCluster  = 0;
	private int numeroCluster;
	private ArrayList<String> listClass;
	private float poids = 0;
	
	
	//ca marche par copie ?
	public Cluster(ArrayList<String> list) {
		listClass = list;
		numeroCluster = nbTotalCluster;
		++nbTotalCluster;
	}
	
	public Cluster() {
		listClass = new ArrayList<String>();
		numeroCluster = nbTotalCluster;
		++nbTotalCluster;
	}
	
	public Cluster(KeyCouple couple) {
		listClass = new ArrayList<String>();
		listClass.add(couple.getSource());
		listClass.add(couple.getDestination());
		
		numeroCluster = nbTotalCluster;
		++nbTotalCluster;
	}
	
	public Cluster(Cluster c1, Cluster c2) {
		listClass = new ArrayList<String>();
		for(String s : c1.getListClass()) {
			listClass.add(s);
		}
		for(String s : c2.getListClass()) {
			listClass.add(s);
		}
		numeroCluster = nbTotalCluster;
		++nbTotalCluster;
	}

	/////////////////////////////////////////////////////////////////////////////////////////
	// il faut retirer les couples deja utilise au clustering precedent
	// ce calcul se fait lors du clustering, ainsi seuls les appels pertinants au clustering actuel sont disponibles
	//lors de la fiun du clustering tous les couples utilises sont retires de la liste
	public void calculPoids(int nbTotalCall, ArrayList<Couple> listPonderation) {
		float nbCall = 0;
		for(Couple c : listPonderation) {
			if(listClass.contains(c.getCouple().getSource()) && listClass.contains(c.getCouple().getDestination())) {
				nbCall += c.getPoids();
			}
		}
		if(nbCall>0) nbCall = nbCall/nbTotalCall;
		poids = nbCall;
	}

	//
	public void addClassToCluster(String s) {
		listClass.add(s);
	}
	
	public boolean containsClass(String s) {
		return listClass.contains(s);
	}
	
	//
	
	public static int getNbTotalCluster() {
		return nbTotalCluster;
	}
	
	public ArrayList<String> getListClass() {
		return listClass;
	}

	public int getNumeroCluster() {
		return numeroCluster;
	}

	public float getPoids() {
		return poids;
	}

	public void setPoids(float poids) {
		this.poids = poids;
	}

	
}
