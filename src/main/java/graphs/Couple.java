package graphs;

public class Couple {
	private KeyCouple couple;
	private float poids;
	
	public Couple(KeyCouple couple, float poids) {
		super();
		this.couple = couple;
		this.poids = poids;
	}

	public KeyCouple getCouple() {
		return couple;
	}

	public void setCouple(KeyCouple couple) {
		this.couple = couple;
	}

	public float getPoids() {
		return poids;
	}

	public void setPoids(int poids) {
		this.poids = poids;
	}
	
	
}
