package graphs;

public class KeyCouple {
	String source;
	String destination;
	
	public KeyCouple(String source, String destination) {
		super();
		this.source = source;
		this.destination = destination;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof KeyCouple)) return false;
        KeyCouple key = (KeyCouple) o;
        if(this.source == null || this.destination == null || key.getSource() == null || key.getDestination() == null) return false;
        return source.equals(key.getSource()) && destination.equals(key.getDestination());
    }

    @Override
    public int hashCode() {
    	final int prime = 31;
		int result = 1;
		result = prime * result + ((source == null) ? 0 : source.hashCode());
		result = prime * result + ((destination == null) ? 0 : destination.hashCode());
		return result;
    }
	
	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}
	
	
	
}
